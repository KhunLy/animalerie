﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animalerie
{
    public enum Caractere
    {
        Farouche, Energique, Calin, Docile
    }

    public class Chat : Animal
    {
        public Caractere Caractere { get; set; }
        public bool Griffe { get; set; }
        public bool PoilLong { get; set; }
        public override double ProbabiliteDeces
        {
            get { return 0.005; }
        }
    }
}
