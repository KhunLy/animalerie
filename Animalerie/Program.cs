﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Animalerie
{
    internal class Program
    {
        static List<Animal> animaux = new List<Animal>();

        static void Main(string[] args)
        {
            string choix = null;
            while (choix != "5")
            {
                choix = Question("Que voulez-vous faire?\n1.Encoder un animal\n2.Voir les animaux\n3.Afficher Quantités\n4.Vérifier les déces\n5.Quitter");
                switch (choix)
                {
                    case "1":
                        EncoderUnAnimal();
                        break;
                    case "2":
                        AfficherLesAnimaux();
                        Console.ReadKey();
                        break;
                    case "3":
                        AfficherLesQuantites();
                        break;
                    case "4":
                        TesterDeces();
                        break;
                }
            }
        }

        static void TesterDeces()
        {
            Console.Clear();
            List<Animal> aSupprimer = new List<Animal>();
            foreach (Animal a in animaux)
            {
                Random random = new Random();
                int value = random.Next(0, 100);
                if(value < a.ProbabiliteDeces * 100)
                {
                    aSupprimer.Add(a);
                }
            }
            foreach (Animal a in aSupprimer)
            {
                Console.WriteLine($"{a.Nom} est décédé");
                animaux.Remove(a);
            }
            Console.ReadKey();
        }

        static void AfficherLesQuantites()
        {
            Console.Clear();
            int chiens = 0;
            int chats = 0;
            int oiseaux = 0;
            foreach (Animal animal in animaux)
            {
                if (animal is Chat)
                {
                    chats++;
                }
                else if (animal is Chien)
                {
                    chiens++;
                }
                else if (animal is Oiseau)
                {
                    oiseaux++;
                }
            }
            #region Linq
            //int chats = animaux.Count(a => a is Chat);
            //int chiens = animaux.Count(a => a is Chien);
            //int oiseaux = animaux.Count(a => a is Oiseau); 
            #endregion
            Console.WriteLine($"Chat: {chats}");
            Console.WriteLine($"Chien: {chiens}");
            Console.WriteLine($"Oiseau: {oiseaux}");
            Console.ReadKey();
        }

        static void AfficherLesAnimaux()
        {
            Console.Clear();
            Console.WriteLine($"Type\tNom\tSexe\tDescription");
            Console.WriteLine($"----\t---\t----\t-----------");
            foreach (Animal animal in animaux)
            {
                Console.Write($"{animal.GetType().Name}\t{animal.Nom}\t{animal.Sexe}");
                if(animal is Chat chat)
                {
                    Console.WriteLine($"\t{chat.Caractere}");
                }
                if(animal is Chien chien)
                {
                    Console.WriteLine($"\t{chien.Race}");
                }
                if(animal is Oiseau oiseau)
                {
                    Console.WriteLine($"\t{oiseau.Couleur}");
                }
            }
        }

        static void EncoderUnAnimal()
        {
            string nom = Question("Nom?");
            double poids = double.Parse(Question("Poids?"));
            double taille = double.Parse(Question("Taille?"));
            Genre sexe = (Genre)int.Parse(Question("Sexe?\n0.Feminin\n1.Masculin\n2.Autre"));
            int age = int.Parse(Question("Age?"));

            switch(int.Parse(Question("Choisir un type:\n1.Chat\n2.Chien\n3.Oiseau")))
            {
                case 1:
                    Caractere caractere = (Caractere)int.Parse(Question("Caractère?\n0.Farouche\n1.Energique\n2.Calin\n3.Docile"));
                    bool griffe = bool.Parse(Question("Griffe?"));
                    bool poilLong = bool.Parse(Question("Poil Long?"));

                    Chat chat = new Chat();
                    chat.Nom = nom;
                    chat.Poids = poids;
                    chat.Taille = taille;
                    chat.Age = age;
                    chat.Sexe = sexe;
                    chat.PoilLong = poilLong;
                    chat.Griffe = griffe;
                    chat.DateArrivee = DateTime.Now;
                    chat.Caractere = caractere;
                    animaux.Add(chat);
                    break;
                case 2:
                    string couleurCollier = Question("Couleur Collier?");
                    string race = Question("Race?");
                    bool dresse = bool.Parse(Question("Le chien est il dressé?"));

                    Chien chien = new Chien();
                    chien.Nom = nom;
                    chien.Poids = poids;
                    chien.Taille = taille;
                    chien.Age = age;
                    chien.Sexe = sexe;
                    chien.CouleurCollier = couleurCollier;
                    chien.Race = race;
                    chien.EstDresse = dresse;
                    chien.DateArrivee = DateTime.Now;
                    animaux.Add(chien);
                    break;
                case 3:
                    string couleur = Question("Couleur?");
                    bool enCage = bool.Parse(Question("En cage ?"));

                    Oiseau oiseau = new Oiseau();
                    oiseau.Nom = nom;
                    oiseau.Poids = poids;
                    oiseau.Taille = taille;
                    oiseau.Age = age;
                    oiseau.Sexe = sexe;
                    oiseau.Couleur = couleur;
                    oiseau.VivreCage = enCage;
                    oiseau.DateArrivee = DateTime.Now;
                    animaux.Add(oiseau);
                    break;
            }
        }

        static string Question(string message)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
            return Console.ReadLine();
        }
    }
}
