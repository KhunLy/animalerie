﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animalerie
{
    public class Oiseau : Animal
    {
        public string Couleur { get; set; }
        public bool VivreCage { get; set; }
        public override double ProbabiliteDeces
        {
            get { return 0.03; }
        }
    }
}
