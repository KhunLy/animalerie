﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animalerie
{
    public class Chien : Animal
    {
        public string CouleurCollier { get; set; }
        public bool EstDresse { get; set; }
        public string Race { get; set; }
        public override double ProbabiliteDeces
        {
            get { return 0.01; }
        }
    }
}
