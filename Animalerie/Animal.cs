﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animalerie
{
    public enum Genre
    {
        Feminin, Masculin, Autre
    } 

    public abstract class Animal
    {
        public string Nom { get; set; }
        public double Poids { get; set; }
        public double Taille { get; set; }
        public Genre Sexe { get; set; }
        public int AgeHumain { get; set; }
        public int Age { get; set; }
        public DateTime DateArrivee { get; set; }
        public bool EstDecede { get; set; }
        public abstract double ProbabiliteDeces { get; }
        public void Crier()
        {
            Console.WriteLine($"Coucou, je m'appelle {Nom}");
        }
    }
}
